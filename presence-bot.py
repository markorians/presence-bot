#!/usr/bin/env python3

import discord, sqlite3, datetime, json, sys, syslog

from discord.ext import commands

class DB( object ):
	connection = None
	def __init__( self, filename ):
		self.connection = sqlite3.connect( filename )
	def connect( self, filename ):
		self.__init__( self, filename )
	def execute( self, stmt, t=None ):
		cur = self.connection.cursor()
		if t == None:
			cur.execute( stmt )
		else:
			cur.execute( stmt, t )
		self.connection.commit()
		return cur.fetchall()
	def close( self ):
		self.connection.close()
		self.connection = None
	def initDB( self ):
		if self.connection is not None:
			cur = self.connection.cursor()
			cur.executescript('''
				CREATE TABLE IF NOT EXISTS presence_log (
					id INTEGER PRIMARY KEY AUTOINCREMENT,
					id_guild TEXT,
					id_player TEXT,
					guild TEXT,
					player TEXT,
					status TEXT,
					date DATETIME
				);
				CREATE TABLE IF NOT EXISTS settings (
					key TEXT UNIQUE,
					value TEXT
				);
				CREATE INDEX IF NOT EXISTS idx_guilds ON presence_log( guild );
				CREATE INDEX IF NOT EXISTS idx_players ON presence_log( player );
				INSERT INTO settings( key, value ) SELECT "prefix", "$" WHERE NOT EXISTS ( SELECT 1 FROM settings WHERE key = "prefix" );
				INSERT INTO settings( key, value ) SELECT "gmt", "0" WHERE NOT EXISTS ( SELECT 1 FROM settings WHERE key = "gmt" );
			''')
			self.connection.commit()


class Presence(commands.Cog):
	dbs = {}
	def __init__(self, bot):
		self.bot = bot
		self._last_member = None

	@commands.Cog.listener()
	async def on_ready(self):
		print( '[+] BOT Started.' )
		print( '[+] User Name : {0}'.format( self.bot.user.name ) )
		print( '[+] User ID   : {0}\n'.format( self.bot.user.id   ) )

		print( '[+] Scanning Guilds ...\n' )
		for s in self.bot.guilds:
			print( '[++] Found Guild "{0}"'.format( s ) )
			print( '[++] Scanning channels ...\n' )

			self.dbs[str(s.id)] = DB( "dbs/" + str(s.id) + ".db" )
			self.dbs[str(s.id)].initDB()

	@commands.Cog.listener()
	async def on_guild_join(self, guild):
		print( '[+] New Guild {0} !!! ' .format( guild ) )
		if not hasattr( self.dbs, str(guild.id) ):
			print( '[+] No Databse found, creating ...' )
			self.dbs[str(guild.id)] = DB( "dbs/" + str(guild.id) + ".db" )

	@commands.Cog.listener()
	async def on_member_join(self, member):
		channel = member.guild.system_channel
		if channel is not None:
			await channel.send('Welcome {0.mention}.'.format(member))

	@commands.Cog.listener()
	async def on_member_update(self, before, after):
		id_member = before.id
		id_guild  = before.guild.id

		props = [ 'status', 'mobile_status', 'desktop_status', 'web_status' ]
		stats = {}

		for p in props:
			stats[p] = {
				"before" : str( getattr( before, p ) ),
				"after"  : str( getattr( after,  p ) )
			}

		tup = (
			before.guild.id,
			before.id,
			before.guild.name,
			'{0}#{1}'.format( before.name, before.discriminator ),
			str(stats),
		)

		self.dbs[str(id_guild)].execute('''
			INSERT INTO presence_log( id_guild, id_player, guild, player, status, date )
			VALUES ( ?, ?, ?, ?, ?, datetime('now') )
		''', tup );

	@commands.command()
	async def change_prefix(self, ctx, p):
		"""Change BOT Prefix"""
		if len(p) == 1:
			self.dbs[str(ctx.guild.id)].execute( 'REPLACE INTO settings( key, value ) VALUES ( "prefix", ? )', ( p, ) )
			await ctx.send( 'Old Prefix : "{0}" New Prefix : "{1}"'.format( ctx.prefix, p ) )
		else:
			await ctx.send( 'Wrong prefix length' )

	@commands.command()
	async def change_gmt(self, ctx, g):
		"""Change BOT GMT Conversion"""
		if g.replace('-','').replace('+','').isdigit():
			self.dbs[str(ctx.guild.id)].execute( 'REPLACE INTO settings( key, value ) VALUES ( "gmt", ? )', ( g, ) )
			await ctx.send( 'New GMT : "{0}"'.format( g ) )
		else:
			await ctx.send( 'Wrong GMT Value' )

	@commands.command()
	async def get(self, ctx, *args):
		"""Get latest 20 records"""

		msg = ""
		res = []

		q = ""
		t = None

		settings = self.dbs[str(ctx.guild.id)].execute( 'SELECT * FROM settings WHERE key = "gmt"' )

		print("command GET : ", args)

		if len(args) > 0:
			param = args[0]
			if param == "player" and len(args) == 2:
				q = "SELECT date, player, status FROM presence_log WHERE id_player = ? ORDER BY id DESC LIMIT 20"
				t = (args[1][2:-1],)
			if param == "stats"  and len(args) == 2:
				q = 'SELECT DATE(date) AS d,STRFTIME("%H",date) AS h,player,COUNT(*) AS c FROM presence_log WHERE id_player = ? GROUP BY d,h ORDER BY id DESC LIMIT 20'
				t = (args[1][2:-1],)
		else:
			q = 'SELECT date, player, status FROM presence_log ORDER BY id DESC LIMIT 20' 

		res = self.dbs[str(ctx.guild.id)].execute( q, t )

		if len(args) > 0 and args[0] == "stats":
			for r in res:
				d = datetime.datetime.strptime( r[0], "%Y-%m-%d" )
				d = d.replace( hour=int(r[1]), minute=0 )
				if settings[0][1][0] == '-':
					d = d - datetime.timedelta(hours=int(settings[0][1].replace('-','')))
				else:
					d = d + datetime.timedelta(hours=int(settings[0][1].replace('+','')))
				msg += "{0} {1} {2} events\n".format( d, r[2], r[3] )
		else:
			for r in res:
				p = json.loads( r[2].replace( '\'', '\"' ) )
				d = datetime.datetime.strptime( r[0], '%Y-%m-%d %H:%M:%S' )
				if settings[0][1][0] == '-':
					d = d - datetime.timedelta(hours=int(settings[0][1].replace('-','')))
				else:
					d = d + datetime.timedelta(hours=int(settings[0][1].replace('+','')))
				msg += "{0} {1} {2} --> {3}\n".format( d, r[1], p["status"]["before"], p["status"]["after"] )

		await ctx.send( msg )

def prefixes( bot, msg ):
	db = DB( "dbs/" + str(msg.guild.id) + ".db" )
	r = db.execute("SELECT * FROM settings")
	return r[0][1]

syslog.syslog('Presence Bot Started')

try:
	bot = commands.Bot( command_prefix = prefixes )
	presence = Presence( bot )
	bot.add_cog( presence )
	bot.run('NzEzNjgxNzQxODY4Njk1NTc0.XsjroQ.YxEP6da6X2IGi17sj38C9XAIAT4')
except:
	e = sys.exc_info()[0]
	syslog.syslog( "Unhandle Exception: %s" % e )
finally:
	syslog.syslog('Presence Bot End')
